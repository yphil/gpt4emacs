# gpt4emacs

A modern `M-x doctor` for Emacs 😉

This module provides a simple prompt to query the [OpenAI](https://openai.com) API within [Emacs](https://www.gnu.org/software/emacs/), either on the region, the whole buffer, or out of the blue.

![Not in real time 😉](https://yphil.gitlab.io/blog/images/g4e.gif)

Exposes only one single (asynchronous) command, uses the `customize` system to set the [API key](https://platform.openai.com/account/api-keys), the GPT *rules*, the *temperature*, and the *model*, and pays special attention to human languages encodings and compatibility.

## Features
- Optional replacement of the selection
- Customizable `gpt-rules` directive to append to any further query, editable on-the-fly ;
- Optional (searchable / editable) list of previous queries
- Optional display of the token data (Prompt, Completion, Total)
- UTF-8 aware

## Installation
In your init file:
### Package
``` emacs-lisp
(use-package gpt4emacs
  :straight (:type git :url "https://framagit.org/yphil/gpt4emacs.git")
  :config ;; Optional, see below
  (setq gpt-api-key-file "/your/key")
  (setq gpt-model "text-davinci-003")
  (setq gpt-max-tokens 300)
  (setq gpt-temperature 0.0))
```
### Manual
``` emacs-lisp
(load "/path/to/gpt4emacs.el")
```

## Configuration
``` emacs-lisp
M-x customize-group RET gpt4emacs RET
```
There, you can set:
- The path of the file where your [API key](https://platform.openai.com/account/api-keys) lives (**1**);
- The URL of the API (https://api.openai.com/v1/completions);
- The `gpt-rules`, a string to prepend to each subsequent request ("");
- The *temperature* of the query (0.2);
- The maximum number of *tokens* returned (300);
- The displaying of token data in the (messages) minibuffer (on)
- The *model* used (text-davinci-003).

**1**: Alternatively, you can just `export OPENAI_API_KEY=[key]`; but make sure this environment variable is propagated, depending on how you launch Emacs.

### Persistence of the list of queries
To put all your previous queries in a neat, searchable / editable [ivy](https://github.com/abo-abo/swiper/) menu, put this [use-package](https://github.com/jwiegley/use-package) declaration in you init file:

``` emacs-lisp
(use-package ivy
  :bind
  (([(f2) (g)] . gpt4emacs-prompt)
   ([(shift f2) (g)] . gpt4emacs-prompt-ivy)
   :map ivy-minibuffer-map
   ("C-M-w" . ivy-kill-line))
  :init
  (defvar gpt-remove-duplicates t
    "If non-nil, remove duplicates from the minibuffer history in gpt4emacs-prompt-ivy.")

  (defun gpt--remove-duplicates (list)
    "Return a new list that is a copy of LIST with duplicates removed."
    (let ((new-list nil))
      (dolist (item list)
        (unless (member item new-list)
          (push item new-list)))
      (nreverse new-list)))

  (defun gpt4emacs-prompt-ivy (arg)
    "Prompt the user with a query, and send it to Gpt API."
    (interactive "P")
    (let ((history (if gpt-remove-duplicates
                       (gpt--remove-duplicates minibuffer-history)
                     minibuffer-history)))
      (ivy-read "Ask GPT: " history
                :action (lambda (query)
                          (unless (member query minibuffer-history)
                            (add-to-history 'minibuffer-history query))
                          (gpt4emacs-prompt arg query))
                :caller 'gpt4emacs-prompt-ivy))))
```
- Use [savehist](https://www.gnu.org/savannah-checkouts/gnu/emacs/manual/html_node/emacs/Saving-Emacs-Sessions.html) to make the queries list persistent between sessions;
- In Ivy, use `M-i` to insert the current list item for editing on-the-fly.

## Usage
`M-x gpt4emacs-prompt` or `M-x gpt4emacs-prompt-ivy`

- If no region is selected, only the question is asked, and displayed in the gpt-buffer;
- When invoked with a prefix (`C-u`) argument, the response is printed at cursor position (point);
- When invoked with a prefix argument and a region is selected, the region is replaced with the response;
- Otherwise, said response is displayed in a modal `info` buffer - use `q` to close it.
- Use `M-x gpt4emacs-set-rules` to re-define the `gpt-rules` string on-the-fly;

### Tips

- In Ivy mode (see [Configuration](#Configuration)) use `M-i` in the minibuffer to insert the current list item (*candidate*) for immediate editing;
- Even if you don't use Ivy all your previous queries are still available using the `UP` key, just like any input in Emacs;
- And if you use [the savehist (standard) library](https://www.gnu.org/savannah-checkouts/gnu/emacs/manual/html_node/emacs/Saving-Emacs-Sessions.html) which you should, they are persistent between sessions.

---

Brought to you by [yPhil](https://yphil.gitlab.io/blog). Please consider helping ♥ [here](https://liberapay.com/yPhil/donate) or [here](https://ko-fi.com/yphil). Icon by Buuf.
