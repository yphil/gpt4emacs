;;; gpt4emacs.el --- Simple GPT (General Purpose Tool) prompt for Emacs  -*- lexical-binding: t -*-

;; Copyleft (C) yPhil

;; This package is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This package is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;; Author: yPhil <xaccrocheur@gmail.com>
;; Maintainer: yPhil <xaccrocheur@gmail.com>
;; Created: 2023
;; Version: 0.06
;; Package-Requires: ((emacs "28.1"))
;; URL: https://framagit.org/yphil/gpt4emacs
;; License: GPLv3

;;; Commentary:

;; A simple prompt to query the OpenAI API within Emacs either on the region, the whole buffer or standalone ; Made after the *fantastic* tutorial by Gavin Freeborn (https://youtu.be/EgVfurJUdFo) thanks a lot, Gavin!
;; This one is different, with only one single flexible command and a system to ease the API key setting and set the preferences.

;;; Code:

(require 'seq)
(require 'subr-x)
(require 'json)

(defgroup gpt4emacs nil
  "GPT frontend."
  :group 'convenience
  :prefix "gpt-")

(defcustom gpt-max-tokens 300
  "The API will not return more than a certain number of tokens."
  :type 'integer)

(defcustom gpt-temperature 0.0
  "Temperature value for the API request."
  :type 'float)

(defcustom gpt-model "text-davinci-003"
  "Model to be used for the API request."
  :type 'string)

(defcustom gpt-api-key-file "/tmp/clef"
  "The file containing your API key.
A regular text file containing only your API key ; Get one at https://openai.com/."
  :type 'string)

(defcustom gpt-display-token-data t
  "Whether to display token data in the output buffer."
  :type 'boolean)

(defcustom gpt-rules ""
  "An optional string containing rules to prepend to every request."
  :type 'string)

(defcustom gpt-api-url "https://api.openai.com/v1/completions"
  "The URL for the OpenAI API."
  :type 'string
  :group 'gpt)

(defvar gpt-api-key nil
  "The variable to store the API key.")

(defun gpt4emacs-set-rules (new-rules)
  "Set the value of gpt-rules to NEW-RULES."
  (interactive (list (read-string (format "Enter rules (current: %s): " gpt-rules))))
  (setq gpt-rules new-rules)
  (message "GPT rules updated."))

;;;###autoload
(defun gpt--api-key-from-file (file)
  "Set the gpt-api-key variable from the FILE if it is not already set.
If the variable is not set and the file does not exist, display a warning message."
  (unless gpt-api-key
    (if (file-exists-p file)
        (with-temp-buffer
          (insert-file-contents file)
          (setq gpt-api-key (buffer-string)))
      (message "Warning: file not found: %s" file))))

(defvar gpt-buffer "*gpt4emacs  - press q to close*"
  "Name of the buffer used to store the results of an OpenAI API query.")

(defun gpt--display-response (results callback)
  "Display the RESULTS from the OpenAI API and pass the GPT buffer to CALLBACK."
  (let ((text (plist-get (elt results 0) :text))
        (decoded-text nil))
    (with-current-buffer (get-buffer-create gpt-buffer)
      (view-mode -1)
      (erase-buffer)
      (setq decoded-text (decode-coding-string text 'utf-8))
      (insert decoded-text)
      (set-buffer-file-coding-system 'utf-8)
      (goto-char (point-min))
      (when (looking-at-p "\\.")
        (delete-region (point) (1+ (line-end-position))))
      (while (looking-at-p "^\\s-*$")
        (delete-region (point) (1+ (line-end-position))))
      (view-mode 1))
    (funcall callback (get-buffer gpt-buffer))))

(defun gpt--query (prompt callback)
  "Send a query with PROMPT and call CALLBACK with the resulting buffer.
PROMPT is a string containing the query to send.
CALLBACK takes a single argument, the buffer containing the query result."
  (gpt--query-api prompt
                  (lambda (results)
                    (gpt--display-response results callback))))

;;;###autoload
(defun gpt4emacs-prompt (&optional arg query)
  "Prompt the user with a query, and send it to Gpt API.
Process ARG."
  (interactive "P")
  (let* ((region-selected (use-region-p))
         (beg (when region-selected (region-beginning)))
         (end (when region-selected (region-end)))
         (og-buf (current-buffer))
         (current-code (if region-selected (buffer-substring beg end) ""))
         (user-query (or query (read-string "Ask GPT: ")))
         (full-query (concat current-code 
                             (if (string-empty-p current-code) "" "\n\n") 
                             user-query 
                             (if (string-empty-p gpt-rules) "" "\n\n") 
                             gpt-rules)))
    (gpt--query (if (string-empty-p current-code) full-query (concat current-code "\n\n" full-query))
                (if arg
                    (lambda (buf)
                      (with-current-buffer og-buf
                        (if region-selected
                            (progn
                              (delete-region beg end)
                              (goto-char beg))
                          (goto-char (point)))
                        (insert (with-current-buffer buf (buffer-string)))))
                  (lambda (buf)
                    (setq buf buf)  ; Use buf without changing anything
                    (switch-to-buffer-other-window gpt-buffer))))))

(defun gpt--handle-token-data (token-data)
  "Handle token data based on the value of `gpt-display-token-data`.
If `gpt-display-token-data` is non-nil, message the token data.
TOKEN-DATA is a plist containing the token usage information."
  (let ((prompt_tokens (plist-get token-data :prompt_tokens))
        (completion_tokens (plist-get token-data :completion_tokens))
        (total_tokens (plist-get token-data :total_tokens)))
    (when gpt-display-token-data
      (message "Prompt: %d Completion: %d Total tokens: %d"
               prompt_tokens completion_tokens total_tokens))))

(defun gpt--parse-response (status callback)
  "Parse the JSON response from the OpenAI API and pass the result to CALLBACK.
STATUS is a plist containing the request status information.
CALLBACK is a function to be called with the parsed response."
  (if (plist-get status :error)
      (let ((error-message (format "Error: %S" (plist-get status :error))))
        (if (equal (plist-get status :error) '(error http 400))
            (setq error-message (concat error-message " Max token limit exceeded")))
        (message "%s" error-message))
    (with-current-buffer (current-buffer)
      (goto-char (point-min))
      (re-search-forward "^$" nil 'move)
      (let ((content (buffer-substring-no-properties (1+ (point)) (point-max))))
        (condition-case nil
            (let* ((json-object-type 'plist)
                   (json-array-type 'list)
                   (json-key-type 'keyword)
                   (json-false nil)
                   (json-null nil)
                   (json (json-read-from-string content))
                   (choices (plist-get json :choices))
                   (choice (elt choices 0))
                   (text (plist-get choice :text))
                   (token-data (plist-get json :usage)))
              (gpt--handle-token-data token-data)
              (funcall callback (list (plist-put choice :text text))))
          (error
           (message "Error parsing JSON response.")
           (funcall callback nil)))))))

(defun gpt--query-api (prompt callback)
  "Sends a POST request to the GPT API with the given PROMPT.
Parse the response and calls the given CALLBACK with the result.
Prints the request data and extra headers to the *Messages* buffer before sending the request."
  (let ((url-request-method "POST")
        (url-request-extra-headers `(("Content-Type" . "application/json; charset=utf-8")
                                     ("Authorization" . ,(format "Bearer %s" gpt-api-key))))
        (url-request-data (encode-coding-string (json-encode
                                                 `(("model" . ,gpt-model)
                                                   ("prompt" . ,(encode-coding-string prompt 'utf-8))
                                                   ("max_tokens" . ,gpt-max-tokens)
                                                   ("temperature" . ,gpt-temperature))) 'utf-8))
        (coding-system-for-read 'utf-8))
    (if (and gpt-api-key (not (string= "" gpt-api-key)))
        (progn
          (message "Sending request: %s" url-request-data)  ;; Print the request data
          (message "Extra headers: %s" url-request-extra-headers)  ;; Print the extra headers
          (url-retrieve gpt-api-url
                        (lambda (status)
                          (gpt--parse-response status callback))))
      (error "API key is not set or is empty"))))

;;;###autoload
(add-hook 'after-init-hook (lambda () (gpt--api-key-from-file gpt-api-key-file)))

(provide 'gpt4emacs)
;;; gpt4emacs.el ends here
